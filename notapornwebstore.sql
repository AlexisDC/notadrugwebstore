-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 16 juil. 2019 à 19:57
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `notapornwebstore`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `stock` varchar(255) NOT NULL,
  `seller` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`ID`, `name`, `price`, `stock`, `seller`, `path`) VALUES
(1, 'Green Arabica', '20', '7', 'moi', 'images/theVert.jpg'),
(2, 'The blanc', '50', '5', 'admin', 'images/the0.jpg'),
(4, 'Purple Haize', '80', '4', 'someone', 'images/the1.jpg'),
(5, 'Skunk', '70', '6', 'someone', 'images/the2.jpg'),
(6, 'White Widow', '80', '4', 'someone', 'images/the3.jpg'),
(7, 'Sativa', '50', '12', 'someone', 'images/the4.jpg'),
(8, 'Alien Kush', '50', '12', 'someone', 'images/the5.jpg'),
(9, 'Blueberry', '90', '2', 'someone', 'images/the6.jpg'),
(10, 'Grape Ape', '60', '3', 'someone', 'images/the7.jpg'),
(11, 'Big Bud', '60', '3', 'someone', 'images/the8.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `town` varchar(255) NOT NULL,
  `code_postal` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`ID`,`login`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`ID`, `login`, `password`, `name`, `fname`, `phone`, `adress`, `town`, `code_postal`, `deleted`) VALUES
(1, 'alex', 'alex', 'alex', 'alex', 'alex', 'alex', 'alex', 'alex', 0),
(2, 'alex', 'alex', 'alex', 'alex', 'alex', 'alex', 'alex', 'alex', 1),
(3, 'TOM', 'tom', 'tom', 'tom', 'tom', 'tom', 'tom', 'tom', 0),
(4, 'yaya', 'yaya', 'yaya', 'yaya', 'yaya', 'yaya', 'yaya', 'yaya', 0),
(5, 'admin', 'admin', 'admin', 'admin', 'admin', 'admin', 'admin', 'admin', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
