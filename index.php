<html>
<head>
    <title> Not A Drug MarketPlace</title>

    <link rel="stylesheet" type="text/css" href="css/maGodDamnCSS.css">
    <link rel="stylesheet" type="text/css" href="css/backgroundCSS.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    include 'session.php';
    include 'someFunctions.php';
    $logFailed = 0;
    $vide = 1;
    $id = "";
    if(isset($_SESSION['failure'])) {
        if ($_SESSION['failure'] == 0) {
            $logFailed = 3;
            $_SESSION['failure']=1;
        }
        else {
            $logFailed = 1;
        }
    }
    if(isset($_SESSION['vide'])){
        $vide = $_SESSION['vide'];
        $_SESSION['vide'] = 1;
    }

    if(isset($_SESSION['id'])){
        $id = $_SESSION['id'];
    }
    $_SESSION['order']="";
    ?>
    <script>
        var success = <?php echo $logFailed; ?>;
        var vide =  <?php echo $vide; ?>;
    </script>

</head>
<body class="background">

<div class="mainContainer">



    <div id="myModal" class="modal">
        <div class="modal-content" style="width: 20%;">
            <span id="modal" class="close">&times;</span>
            <form  action="authentification.php" method="post">
                <label class="formText">Identifiant</label><br><br>
                <input class="formStyle" type="text" name="login" /><br><br><br>
                <label class="formText">Mot de passe</label><br><br>
                <input class="formStyle" type="password" name="password" /><br><br><br>
                <input class="formButton" type="submit" value="Valider" />
            </form>
        </div>
    </div>

    <div id="confirm" class="modal">
        <div class="modal-content" style="width: 20%;">
            <span id='spanConfirm' class="close">&times;</span>
            <div style="margin-left:20%;margin-right: 50%; min-width:60%;">
                <img class="randomPicture" src="images/valid.png">
            </div>
            <h1 class="h1Text" style="text-align: center;">Produit ajouter au panier !</h1>
        </div>
    </div>

    <div>
        <h1 align="center" class="webName">NotADrugWebstore</h1>
        <br><br>
        <div class="presentation">
            <h1>Bienvenue sur notre site, nous sommes spécialisé dans le thé vert d'Afghanistan</h1>
            <p>Tous nos produits sont importés depuis l'étranger.</p>
        </div>
        <div class="containerConnect">
            <?php
                if($logFailed != 1) {
                    connectButton();
                }
                else {
                    if($id == "5"){ //5 est l'ID de l'admin
                        adminButton();
                    }
                    panierButton();
                }
            ?>
        </div>
    </div>
    <br><br>


    <?php
        $reponse = $bdd->query('SELECT * FROM articles');
        while ($donnees = $reponse->fetch())
        {
            if($donnees['stock'] != 0) {
                appearItems($donnees['name'],$donnees['stock'],$donnees['price'],$donnees['path'],$donnees['ID']);
            }
        }
    ?>


</div>



<script>
    var modal = document.getElementById("myModal");
    var span = document.getElementById("modal");
    var panier = "";
    var valid = document.getElementById("confirm");
    var spanValid = document.getElementById("spanConfirm");

    function modalWindow() {
        modal.style.display = "block";
    }

    function appearValidModal(){
        valid.style.display = "block";
    }

    span.onclick = function() {
        success = 1;
        modal.style.display = "none";
    }

    spanValid.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal ||
            event.target == valid) {
            modal.style.display = "none";
            valid.style.display = "none";
        }
    }

    if(success == 3) {
        alert("Identifiant ou Mot de passe incorrect");
    }

    if(vide == 0) {
        alert("Votre panier est vide");
    }


    function addToCart(id){
        appearValidModal();
        var button = document.getElementById(id);
        var value = button.options[button.selectedIndex].value;
        panier += "_"+value;
        document.getElementsByName("panier")[0].value = panier;
    }

    function redirectAdmin(){
        document.location.href="admin.php";
    }

</script>




</body>
</html>