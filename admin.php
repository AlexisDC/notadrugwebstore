<html>
<head>
    <?php
        include 'databaseConnection.php';
        include 'session.php';
    ?>
    <link rel="stylesheet" type="text/css" href="css/maGodDamnCSS.css">
    <link rel="stylesheet" type="text/css" href="css/backgroundCSS.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
</head>
<body class="background">
    <div class="mainContainer">

        <button class="formButton" onclick="indexRedirection()">Retour accueil</button>
        <br><br><br>
        <h1 class="h1Text" style="text-align: center">Bienvenue sur la page d'administration.</h1>
        <br><br>
        <div class="presentation">
            <p>Vous pouvez soit :</p>
            <ul>
                <li>Gérer les utilisateurs.</li>
                <li>Gérer les articles.</li>
            </ul>
        </div>
        <br><br><br><br>
        <button class="formButton" onclick="userRedirection()" style="display: inline-block; width: 150px;margin-left: 35%">Utilisateurs</button>
        <button class="formButton" onclick="itemRedirection()" style="width:150px;margin-left: 5%;">Articles</button>
    </div>
    <script>
        function indexRedirection(){
            document.location.href="index.php";
        }

        function userRedirection(){
            document.location.href="userControl.php";
        }
        function itemRedirection(){
            document.location.href="items.php";
        }
    </script>

</body>
</html>