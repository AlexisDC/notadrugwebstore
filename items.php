<html>
<head>
    <?php
    include 'databaseConnection.php';
    include 'session.php';

    $id = "";
    if(isset($_SESSION["id"])){
        $id = $_SESSION["id"];
    }
    ?>

    <?php
    $query ="";
    if(isset($_POST["add_name"])){
        $query = "INSERT INTO articles (name,price,stock,seller,path) VALUES";
        $query .= " ('".$_POST['add_name']."','".$_POST['add_price']."','".$_POST['add_stock'] ;
        $query .= "','".$_POST['add_seller']."','".$_POST['add_path']."');";
        $reponse = $bdd->query($query);
    }
    ?>

    <?php
    if (isset($_POST['del_id'])){
        foreach($_POST['del_id'] as $valeur)
        {
            $query = "DELETE FROM articles WHERE ID = ".$valeur;
            $reponse = $bdd->query($query);
        }
    }
    ?>

    <?php
    if(isset($_POST["mod_ID"])){
        $query = "UPDATE  articles SET ";
        $query .= "name = '".$_POST["mod_name"]."', ";
        $query .= "price = '".$_POST["mod_price"]."', ";
        $query .= "stock = '".$_POST["mod_stock"]."', ";
        $query .= "seller = '".$_POST["mod_seller"]."', ";
        $query .= "path = '".$_POST["mod_path"]."' ";
        $query .= " WHERE ID = '".$_POST["mod_ID"]."';";
        $reponse = $bdd->query($query);
    }
    ?>

    <link rel="stylesheet" type="text/css" href="css/maGodDamnCSS.css">
    <link rel="stylesheet" type="text/css" href="css/backgroundCSS.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script>
        var select = [''];
    </script>

</head>
<body class="background">

    <div class="mainContainer">

        <div id="modalAdd" class="modal">
            <div class="modal-content" style="width:20%;">
                <span id="spanAdd" class="close">&times;</span>
                <h1 class="h1Text">Ajout d'un nouvel utilisateur</h1>
                <form  action="items.php" method="post">
                    <table  style="text-align: center;">
                        <tr>
                            <td>
                                <label class="pText">Nom de l'article :</label>
                            </td>
                            <td>
                                <input class="formStyle" type="text" name="add_name" required /><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="pText">Prix en Euro :</label>
                            </td>
                            <td>
                                <input class="formStyle" type="text" name="add_price" required/><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="pText">Quantité :</label>
                            </td>
                            <td>
                                <input class="formStyle" type="text" name="add_stock" required/><br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="pText">Vendeur :</label>
                            </td>
                            <td>
                                <input class="formStyle" type="text" name="add_seller" required/><br>
                            </td>
                        </tr>
                    </table>

                    <br><br><br><br>
                    <input class="formButton" type="submit" value="Ajouter"  style="margin-left: 50%; margin-right: 50%; width: 150px"/>

                </form>
            </div>
        </div>


        <div id="modalModify" class="modal">
            <div class="modal-content">
                <span id="spanModif" class="close">&times;</span>
                <h1 class="h1Text">MODIFY</h1>
                <p class="pText">Choix de l'user a modifier : </p>
                <table style="text-align: center;margin-left:10%;">
                    <tr>
                        <th style="min-width: 50px" class="h1Text"></th>
                        <th style="min-width: 100px" class="h1Text">Nom de l'article</th>
                        <th style="min-width: 100px" class="h1Text">Prix</th>
                        <th style="min-width: 100px" class="h1Text">Quantité</th>
                        <th style="min-width: 100px" class="h1Text">Vendeur</th>
                        <th style="min-width: 100px" class="h1Text">fichier</th>
                    </tr>
                    <?php

                    $reponse = $bdd->query('SELECT * FROM articles');
                    while ($donnees = $reponse->fetch())
                    {
                        echo "<tr><td><input type=\"button\" onclick='displayModifyUsr(".$donnees['ID'].")'  style='border-radius:5em;min-width:25px;'></td>";
                        echo "<td>" . $donnees['name'] . "</td>";
                        echo "<td>" . $donnees['price'] . "</td>";
                        echo "<td>" . $donnees['stock'] . "</td>";
                        echo "<td>" . $donnees['seller'] . "</td>";
                        echo "<td>" . $donnees['path'] . "</td></tr>";

                        echo "<script>select.push('".$donnees['ID']."')</script>";
                        echo "<script>select.push('".$donnees['name']."')</script>";
                        echo "<script>select.push('".$donnees['price']."')</script>";
                        echo "<script>select.push('".$donnees['stock']."')</script>";
                        echo "<script>select.push('".$donnees['seller']."')</script>";
                        echo "<script>select.push('".$donnees['path']."')</script>";
                    }
                    ?>
                </table>
                <br><br>
                <div class="centerTheBlock">
                    <form action="items.php" method="POST">
                        <table>
                            <tr>
                                <td>
                                    <label class="pText">ID :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_ID" required readonly/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText">Nom de l'article :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_name" required /><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText">Prix en Euro :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_price" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText">Quantité :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_stock" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText">Vendeur :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_seller" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText">fichier (plus tard) :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_path" required/><br>
                                </td>
                            </tr>

                        </table>
                        <br><br>
                        <input class="formButton" type="submit" value="Modifier" style="margin-right: 10%;margin-left:75%;width:150px"/>
                    </form>
                </div>
            </div>
        </div>

        <div id="modalDelete" class="modal">
            <div class="modal-content">
                <span id="spanDel" class="close">&times;</span>
                <h1 class="h1Text">DELETE</h1>
                <form action="items.php" method="POST">
                    <table style="text-align: center;margin-left:10%;">
                        <tr>
                            <th style="min-width: 50px" class="h1Text"></th>
                            <th style="min-width: 100px" class="h1Text">Nom de l'article</th>
                            <th style="min-width: 100px" class="h1Text">Prix</th>
                            <th style="min-width: 100px" class="h1Text">Quantité</th>
                            <th style="min-width: 100px" class="h1Text">Vendeur</th>
                            <th style="min-width: 100px" class="h1Text">fichier</th>
                        </tr>

                        <?php

                        $reponse = $bdd->query('SELECT * FROM articles');
                        while ($donnees = $reponse->fetch())
                        {
                            echo "<tr><td><input type=\"checkbox\" name=\"del_id[]\" value=\"" . $donnees['ID'] . "\"></td>";
                            echo "<td>" . $donnees['name'] . "</td>";
                            echo "<td>" . $donnees['price'] . "</td>";
                            echo "<td>" . $donnees['stock'] . "</td>";
                            echo "<td>" . $donnees['seller'] . "</td>";
                            echo "<td>" . $donnees['path'] . "</td></tr>";
                        }

                        ?>
                    </table>
                    </br></br>
                    <input class="formButton" type="submit" value="Supprimer" style="margin-right: 50%;margin-left:45%;width:150px"/>
                </form>
            </div>
        </div>


        <br><br><br>
        <div class="presentation">
            <h1>Bonjour et bienvenue sur la gestion des <b>articles</b></h1>
            <p>Ici, vous pouvez soit choisir :</p>
            <ul>
                <li>D'ajouter un nouvel article.</li>
                <li>Supprimer un article.</li>
                <li>Modifier les informations d'un article.</li>
            </ul>
        </div>
        <br>
        <div style="display: inline-block; margin-left : 45%; margin-top: 7%; margin-right: 50%;">
            <button class="formButton" onclick="modalAdd()" style="min-width: 200px">Ajouter</button>
            <button class="formButton" onclick="modalModify()" style="min-width: 200px">Modifier</button>
            <button class="formButton" onclick="modalDel()" style="min-width: 200px">Supprimer</button>
        </div>
        <button class="formButton" onclick="indexRedirection()">Retour acceuil</button>

    </div>



    <script>
        var add = document.getElementById("modalAdd");
        var del = document.getElementById("modalDelete");
        var modify = document.getElementById("modalModify");

        var spanAdd = document.getElementById("spanAdd");
        var spanDel = document.getElementById("spanDel");
        var spanModif = document.getElementById("spanModif");


        function displayModifyUsr(id){
            document.getElementsByName("mod_ID")[0].value = select[id*6-5];
            document.getElementsByName("mod_name")[0].value = select[id*6-4];
            document.getElementsByName("mod_price")[0].value = select[id*6-3];
            document.getElementsByName("mod_stock")[0].value = select[id*6-2];
            document.getElementsByName("mod_seller")[0].value = select[id*6-1];
            document.getElementsByName("mod_path")[0].value = select[id*6];
        }

        function modalAdd() {
            add.style.display = "block";
        }

        function modalDel() {
            del.style.display = "block";
        }

        function modalModify() {
            modify.style.display = "block";
        }

        spanAdd.onclick = function() {
            add.style.display = "none";
        }

        spanDel.onclick = function() {
            del.style.display = "none";
        }

        spanModif.onclick = function() {
            modify.style.display = "none";
        }

        window.onclick = function(event) {
            if (event.target == add ||
                event.target == del ||
                event.target == modify) {

                add.style.display = "none";
                del.style.display = "none";
                modify.style.display = "none";
            }
        }
        function indexRedirection(){
            document.location.href="index.php";
        }
    </script>








</body>
</html>