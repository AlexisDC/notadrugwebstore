<html>
<head>
    <?php
        include 'session.php';
        include 'someFunctions.php';
    ?>

    <link rel="stylesheet" type="text/css" href="css/maGodDamnCSS.css">
    <link rel="stylesheet" type="text/css" href="css/backgroundCSS.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
        $order = "";
        $toDelete = "";
        $areDatasHere = 0;
        $idToDelete = "";
        if(isset($_POST["panier"])){
            if($_POST["panier"] == ""){
                header('Location: index.php');
                $_SESSION['vide'] = 0;
            }
            $order = $_POST["panier"];
            $areDatasHere = 1;
        }
        if(isset($_POST['deleteFromPanier']) && isset($_SESSION['order'])){
            $areDatasHere = 2;
            $order = $_SESSION['order'];
            $idToDelete = $_POST['deleteFromPanier'];
            $order = str_replace($idToDelete,"",$order);
        }

        if($order == ""){
            $areDatasHere = 0;
            $_SESSION['vide'] = 0;
        }

        if($areDatasHere == 0) {
            header('Location: index.php');
        }


        $_SESSION['order'] = $order;
    ?>

</head>
<body class="background">

    <div  class="mainContainer" id="mainContainer">
        <div id="myModal" class="modal">
            <div class="modal-content" style="width: 20%;">
                <span class="close">&times;</span>
                <form  action="panier.php" method="post">
                    <input type="hidden" name="deleteFromPanier" />
                    <h1>Etes vous sur de vouloir Supprimer cet article ??</h1>
                    <input class="formButton" type="submit" value="Valider" style="display: inline-block; margin-left:28%;"/>
                    <button class="formButton" onclick="cancelDeletion()" style="display: inline-block;">Annuler</button>
                </form>
            </div>
        </div>

        <h1>Mon panier :</h1>
        <button class="formButton" style="display: inline-block;margin-left: 80%;width:150px;margin-top:-40px;" onclick="indexRedirection()">Retour aux achats</button>
        <br><br>
        <table style="text-align: center;"  id="customers">
                <tr>
                    <th style="width: 250px;">Produit</th>
                    <th>Quantité</th>
                    <th>Prix</th>
                    <th>Supprimer</th>
                </tr>
                <?php
                    $idItem = "";
                    $priceItem = "";
                    $nameItem ="";
                    $pathItem = "";
                    $totalPrice = 0;
                    $dontWrite = 0;
                    $idPlusStockArray = explode("_",$order);


                    for($i = 1; $i < count($idPlusStockArray) ; $i++){
                        $idPlusStock = explode("$",$idPlusStockArray[$i]);
                        $query = "SELECT * FROM articles WHERE id = '".$idPlusStock[0]."';";

                        $reponse = $bdd->query($query);
                        while ($donnees = $reponse->fetch())
                        {
                            if( $donnees['ID'] != $idToDelete) {
                                $idItem = $donnees['ID'];
                                $priceItem = $donnees['price'];
                                $nameItem = $donnees['name'];
                                $pathItem = $donnees['path'];
                                $totalPrice += $priceItem * $idPlusStock[1];
                            }
                            else {
                                $dontWrite = 1;
                            }
                        }
                        if($dontWrite == 0) {
                            echo "<tr>";
                            echo "<td><div class='billName'>" . $nameItem . "</div></div></td>";
                            echo "<td>" . $idPlusStock[1] . "</td>";
                            echo "<td>" . $priceItem * $idPlusStock[1] . "</td>";
                            echo "<td><button style='border-radius:5em;min-width:25px;min-height: 25px;' onclick='deleteFromCart(".$idItem.",".$idPlusStock[1].")'></button></td></tr>";
                        }
                        else {
                            $dontWrite = 0;
                        }
                    }
                ?>
        </table>
        <br><br>
        <h3>Prix total : <?php echo $totalPrice ?></h3>

    </div>
    <br><br><br><br><br><br><br><br><br><br>

    <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
    <script>paypal.Buttons().render('body');</script>



    <script>
        var modal = document.getElementById("myModal");
        var span = document.getElementsByClassName("close")[0];

        span.onclick = function() {
            success = 1;
            modal.style.display = "none";
        }

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }


        function deleteFromCart(id,stock){
            var deletion  = "_"+id+"$"+stock;
            document.getElementsByName("deleteFromPanier")[0].value = deletion;
            modal.style.display = "block";
        }

        function cancelDeletion() {
            modal.style.display = "none";
        }

        function indexRedirection(){
            document.location.href="index.php";
        }
    </script>




</body>
</html>