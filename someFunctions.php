
<?php
    include 'databaseConnection.php';


    function appearItems($nom, $stock, $price,$path,$id){
        echo "<div class=\"itemsDisplay\">";
        echo "<div style='min-height: 60%;'>";
        echo "<img src=\"".$path."\" class=\"itemPicture\">";
        echo "</div>";
        echo "<div style='margin-bot:4%;'>";
        echo "<p class=\"itemName\">".$nom."</p>";
        echo "<p class=\"itemStock\">Stock : ".$stock."</p>";
        echo "<p class=\"itemPrice\">".$price."€</p>";
        echo "<br>";
        echo "<div class=\"listDeroulante\">";
        echo "<select name=\"statut\" id=\"".$id."\">";
        for($i=1; $i <= $stock; $i++){
            echo "<option value=\"".$id."$".$i."\">".$i."</option>";
        }
        echo "</select>";
        echo "</div>";
        echo "<button class=\"itemButton\" onclick='addToCart(".$id.")'>Ajouter au panier</button>";
        echo "</div>";
        echo "</div>";
    }

    function connectButton(){
        echo "<button class=\"connectButton\" id=\"myBtn\" onclick=\"modalWindow()\">Connectez-vous</button>";
    }

    function panierButton(){
        echo "<form action=\"panier.php\" method=\"post\">";
        echo "<input type=\"hidden\" name=\"panier\" />";
        echo "<input class=\"connectButton\" type=\"submit\" value=\"Mon Panier\" />";
        echo "</form>";
    }

    function adminButton() {
        echo "<form action=\"admin.php\" method=\"post\">";
        echo "<input type=\"hidden\" name=\"admin\" />";
        echo "<button class=\"connectButton\" style='display:inline-block;' id=\"myBtn\" onclick=\"redirectAdmin()\">Gestion administrateur</button>";
        echo "</form>";
    }
?>

