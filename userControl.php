<html>
<head>
    <?php
    include 'databaseConnection.php';
    include 'session.php';

    $id = "";
    if(isset($_SESSION["id"])){
        $id = $_SESSION["id"];
    }
    ?>

    <?php
        $query ="";
        if(isset($_POST["add_login"])){
            $query = "INSERT INTO users (login,password,name,fname,phone,adress,town,code_postal,deleted) VALUES";
            $query .= " ('".$_POST['add_login']."','".$_POST['add_password']."','".$_POST['add_name'] ;
            $query .= "','".$_POST['add_fname']."','".$_POST['add_tel']."','".$_POST['add_address'];
            $query .="','".$_POST['add_town']."','".$_POST['add_cp']."','0');";
            $reponse = $bdd->query($query);
        }
    ?>

    <?php
        if (isset($_POST['del_id'])){
            foreach($_POST['del_id'] as $valeur)
            {
                $query = "UPDATE users SET deleted = '1' WHERE ID = ".$valeur;
                $reponse = $bdd->query($query);
            }
        }
    ?>

    <?php
        if(isset($_POST["mod_ID"])){
            $query = "UPDATE  users SET ";
            $query .= "login = '".$_POST["mod_login"]."', ";
            $query .= "password = '".$_POST["mod_password"]."', ";
            $query .= "name = '".$_POST["mod_name"]."', ";
            $query .= "fname = '".$_POST["mod_fname"]."', ";
            $query .= "phone = '".$_POST["mod_tel"]."', ";
            $query .= "adress = '".$_POST["mod_address"]."', ";
            $query .= "town = '".$_POST["mod_town"]."', ";
            $query .= "code_postal = '".$_POST["mod_cp"]."' ";
            $query .= " WHERE ID = '".$_POST["mod_ID"]."';";
            $reponse = $bdd->query($query);
        }
    ?>

    <link rel="stylesheet" type="text/css" href="css/maGodDamnCSS.css">
    <link rel="stylesheet" type="text/css" href="css/scrollBar.css">
    <link rel="stylesheet" type="text/css" href="css/backgroundCSS.css">
    <script>
        var select = [''];
    </script>

</head>
<body class="background">

<div class="mainContainer">

    <div id="modalAdd" class="modal">
        <div class="modal-content" style="width:20%;">
            <span id="spanAdd" class="close">&times;</span>
            <h1 class="h1Text">Ajout d'un nouvel utilisateur</h1>
            <br><br>
            <form  action="userControl.php" method="post">
                <table style="text-align: center;">
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Identifiant :</label>
                        </td>
                        <td>
                            <input class="formStyle" type="text" name="add_login" required /><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Mot de passe :</label>
                        </td>
                        <td>
                            <input class="formStyle" type="text" name="add_password" required/><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Nom :</label>
                        </td>
                        <td>
                            <input class="formStyle" type="text" name="add_name" required/><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Prenom :</label>

                        <td>
                            <input class="formStyle" type="text" name="add_fname" required/><br>
                    </tr>
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Numéro Tel. :</label>

                        <td>
                            <input class="formStyle" type="text" name="add_tel" required/><br>
                    </tr>
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Adresse :</label>
                        </td>
                        <td>
                            <input class="formStyle" type="text" name="add_address" required/><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Ville :</label>
                        </td>
                        <td>
                            <input class="formStyle" type="text" name="add_town" required/><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="pText" style="font-weight: bold">Code postal :</label>
                        </td>
                        <td>
                            <input class="formStyle" type="text" name="add_cp" required/><br>
                        </td>
                    </tr>
                </table>

                <br><br><br><br>
                <input class="formButton" type="submit" value="Ajouter" style="margin-left: 50%; margin-right: 50%; width: 150px" />

            </form>
        </div>
    </div>



    <div id="modalModify" class="modal">
        <div class="modal-content">
            <span id="spanModif" class="close">&times;</span>
            <h1 class="h1Text">MODIFY</h1>
                <p class="pText">Choix de l'user a modifier : </p>
                <table style="text-align: center;margin-left:10%;">
                    <tr>
                        <th style="min-width: 50px" class="h1Text"></th>
                        <th style="min-width: 100px" class="h1Text">Nom</th>
                        <th style="min-width: 100px" class="h1Text">Prenom</th>
                        <th style="min-width: 100px" class="h1Text">Identifiant</th>
                        <th style="min-width: 100px" class="h1Text">Mot de passe</th>
                        <th style="min-width: 100px" class="h1Text">Tel.</th>
                        <th style="min-width: 100px" class="h1Text">Adresse</th>
                        <th style="min-width: 100px" class="h1Text">Ville</th>
                        <th style="min-width: 100px" class="h1Text">Code postal</th>
                    </tr>
                    <?php

                        $reponse = $bdd->query('SELECT * FROM users');
                        while ($donnees = $reponse->fetch())
                        {
                            if($donnees['deleted'] == '0') {
                                echo "<tr><td><input type=\"button\" onclick='displayModifyUsr(".$donnees['ID'].")'  style='border-radius:5em;min-width:25px;'></td>";
                                echo "<td>" . $donnees['name'] . "</td>";
                                echo "<td>" . $donnees['fname'] . "</td>";
                                echo "<td>" . $donnees['login'] . "</td>";
                                echo "<td>" . $donnees['password'] . "</td>";
                                echo "<td>" . $donnees['phone'] . "</td>";
                                echo "<td>" . $donnees['adress'] . "</td>";
                                echo "<td>" . $donnees['town'] . "</td>";
                                echo "<td>" . $donnees['code_postal'] . "</td></tr>";
                            }
                            echo "<script>select.push('".$donnees['ID']."')</script>";
                            echo "<script>select.push('".$donnees['login']."')</script>";
                            echo "<script>select.push('".$donnees['password']."')</script>";
                            echo "<script>select.push('".$donnees['name']."')</script>";
                            echo "<script>select.push('".$donnees['fname']."')</script>";
                            echo "<script>select.push('".$donnees['phone']."')</script>";
                            echo "<script>select.push('".$donnees['adress']."')</script>";
                            echo "<script>select.push('".$donnees['town']."')</script>";
                            echo "<script>select.push('".$donnees['code_postal']."')</script>";
                        }
                     ?>
                </table>
                <br><br>
                <div class="centerTheBlock">
                    <form action="userControl.php" method="POST">
                        <table>
                            <tr>
                                <td style="width: 200px;">
                                    <label class="pText" style="font-weight: bold">ID :</label>
                                </td>
                                <td style="width: 200px;">
                                    <input class="formStyle" type="text" name="mod_ID" required readonly/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Identifiant :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_login" required /><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Mot de passe :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_password" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Nom :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_name" required/><br>
                                </td>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Prenom :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_fname" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Numéro Tel. :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_tel" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Adresse :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_address" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Ville :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_town" required/><br>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="pText" style="font-weight: bold">Code postal :</label>
                                </td>
                                <td>
                                    <input class="formStyle" type="text" name="mod_cp" required/><br>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <br><br>
                    <input class="formButton" type="submit" value="Modifier" style="margin-right: 10%;margin-left:75%;width:150px"/>
                </form>
            <br><br><br><br>
        </div>
    </div>

    <div id="modalDelete" class="modal">
        <div class="modal-content" style="width:60%">
            <span id="spanDel" class="close">&times;</span>
            <h1 class="h1Text">Choisissez l'utilisateur à supprimer.</h1>
            <form action="userControl.php" method="POST">
                <table style="text-align: center;margin-left:10%;">
                    <tr>
                        <th style="min-width: 50px" class="h1Text"></th>
                        <th style="min-width: 100px" class="h1Text">Nom</th>
                        <th style="min-width: 100px" class="h1Text">Prenom</th>
                        <th style="min-width: 100px" class="h1Text">Identifiant</th>
                        <th style="min-width: 100px" class="h1Text">Mot de passe</th>
                        <th style="min-width: 100px" class="h1Text">Tel.</th>
                        <th style="min-width: 100px" class="h1Text">Adresse</th>
                        <th style="min-width: 100px" class="h1Text">Ville</th>
                        <th style="min-width: 100px" class="h1Text">Code postal</th>
                    </tr>

                    <?php

                    $reponse = $bdd->query('SELECT * FROM users');
                    while ($donnees = $reponse->fetch())
                    {
                        if($donnees['ID'] != $id ){
                            if($donnees['deleted'] == '0') {
                                echo "<tr><td><input type=\"checkbox\" name=\"del_id[]\" value=\"" . $donnees['ID'] . "\"></td>";
                                echo "<td>" . $donnees['name'] . "</td>";
                                echo "<td>" . $donnees['fname'] . "</td>";
                                echo "<td>" . $donnees['login'] . "</td>";
                                echo "<td>" . $donnees['password'] . "</td>";
                                echo "<td>" . $donnees['phone'] . "</td>";
                                echo "<td>" . $donnees['adress'] . "</td>";
                                echo "<td>" . $donnees['town'] . "</td>";
                                echo "<td>" . $donnees['code_postal'] . "</td></tr>";
                            }
                        }
                    }

                    ?>
                </table>
                </br></br>
                <input class="formButton" type="submit" value="Supprimer" style="margin-right: 50%;margin-left:45%;width:150px"/>
            </form>
        </div>
    </div>

    <br><br><br>
    <div class="presentation">
        <h1>Bonjour et bienvenue sur la gestion des <b>utilisateurs</b></h1>
        <p>Ici, vous pouvez soit choisir :</p>
        <ul>
            <li>D'ajouter un nouvel utilisateur.</li>
            <li>Supprimer un utilisateur.</li>
            <li>Modifier les informations d'un utilisateur.</li>
        </ul>
    </div>
    <br>
    <div style="display: inline-block; margin-left : 45%; margin-top: 7%; margin-right: 50%;">
        <button class="formButton" onclick="modalAdd()" style="min-width: 200px">Ajouter</button>
        <button class="formButton" onclick="modalModify()" style="min-width: 200px">Modifier</button>
        <button class="formButton" onclick="modalDel()" style="min-width: 200px">Supprimer</button>
    </div>
    <button class="formButton" onclick="indexRedirection()">Retour acceuil</button>

</div>


<script>
    var add = document.getElementById("modalAdd");
    var del = document.getElementById("modalDelete");
    var modify = document.getElementById("modalModify");

    var spanAdd = document.getElementById("spanAdd");
    var spanDel = document.getElementById("spanDel");
    var spanModif = document.getElementById("spanModif");


    function displayModifyUsr(id){
        document.getElementsByName("mod_ID")[0].value = select[id*9-8];
        document.getElementsByName("mod_login")[0].value = select[id*9-7];
        document.getElementsByName("mod_password")[0].value = select[id*9-6];
        document.getElementsByName("mod_name")[0].value = select[id*9-5];
        document.getElementsByName("mod_fname")[0].value = select[id*9-4];
        document.getElementsByName("mod_tel")[0].value = select[id*9-3];
        document.getElementsByName("mod_address")[0].value = select[id*9-2];
        document.getElementsByName("mod_town")[0].value = select[id*9-1];
        document.getElementsByName("mod_cp")[0].value = select[id*9];
    }

    function modalAdd() {
        add.style.display = "block";
    }

    function modalDel() {
        del.style.display = "block";
    }

    function modalModify() {
        modify.style.display = "block";
    }

    spanAdd.onclick = function() {
        add.style.display = "none";
    }

    spanDel.onclick = function() {
        del.style.display = "none";
    }

    spanModif.onclick = function() {
        modify.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == add ||
            event.target == del ||
            event.target == modify) {

            add.style.display = "none";
            del.style.display = "none";
            modify.style.display = "none";
        }
    }

    function indexRedirection(){
        document.location.href="index.php";
    }
</script>

</body>
</html>